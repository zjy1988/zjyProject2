import React, { Component } from 'react';

import {Switch,Route,Redirect} from 'react-router-dom'

import Table from '../Table'
import  Search  from '../Search';

import {connect} from 'react-redux'
import { filter_data,search_data } from '../../redux/antions';


export class App extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    render() {
        return (  
          <div>
              <Search Searchdata={(v)=>{
                  this.props.dispatch(search_data(v))
              }}/>
              <Table  datas={this.props.Defaultdata} filter={(smallfilter)=>{
                  this.props.dispatch(filter_data(smallfilter))
              }}/>
          </div>
         )
    }
}

const filter = (data,filters) =>{
    switch(filters){
        case 'single':
        return data.filter((val) => val.id%2 !== 0)
        break;
        case 'double':
        return data.filter((val) => val.id%2 === 0)
        break;
        default:
            return data;
    }
}
const datas=(state)=>{
    return {
        Defaultdata:filter(state.Defaultdata,state.Changefilter)
    };
}


export default connect(datas)(App);