import { Table } from 'antd';
import React, { Component } from 'react'

export class Tables extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    single (filter){
        this.props.filter(filter)
    }
    render() { 
        const columns = [
            {
                title:'Id',
                dataIndex:'id'
            },
            {
                title: 'Name',
                dataIndex: 'name',
            },
            {
                title: 'Age',
                dataIndex: 'age',
            }, 
            {
                title: 'Address',
                dataIndex: 'address',
            }];
       
      
          
        return ( <div>
            <button onClick = {this.single.bind(this,'single')}>id的奇数渲染</button>
            <button onClick = {this.single.bind(this,'double')}>id的偶数渲染</button>
            <button onClick = {this.single.bind(this,'all')}>id的全部数渲染</button>
            <Table columns = {columns} dataSource={this.props.datas} size = "middle" /> </div>)
    }
}
 
export default Tables;