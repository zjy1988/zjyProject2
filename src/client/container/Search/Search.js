import React, { Component } from 'react'

export class Search extends Component {
    constructor(props) {
        super(props);
        this.state = {  }
    }
    search() {
        let val =this.myInput.value;
        if(val!=''){
            this.props.Searchdata(val)
        }
    }
    render() { 
        return ( 
            <div className="search">
                <input type='text' ref={(input)=>{this.myInput=input}}/>
                <button onClick={this.search.bind(this)}>搜索</button>
            </div>
         )
    }
}
 
export default Search;