import React from 'react'
import ReactDOM from 'react-dom'
import {BrowserRouter as Router} from 'react-router-dom'

import App from './container/App';
import {Provider} from 'react-redux'
import {createStore} from 'redux'

import reducer from './redux/reducer.js'

const store=createStore(reducer);

ReactDOM.render(
    <Provider store={store}>
        <Router>
            <App />
        </Router>
    </Provider>
    ,
    document.getElementById("app")
)