 import {FILTER,SEARCH} from './consts'

 export const filter_data = (filter) => {
    return {
        type:FILTER,
        filter
    }   
 }

 export const search_data = (search) =>{
     return{
         type:SEARCH,
         search
     }
 }